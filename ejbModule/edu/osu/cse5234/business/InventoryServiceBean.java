package edu.osu.cse5234.business;

import edu.osu.cse5234.business.view.Inventory;
import edu.osu.cse5234.business.view.InventoryService;
import edu.osu.cse5234.business.view.Item;
import edu.osu.cse5234.business.view.LineItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.*;

/**
 * Session Bean implementation class InventoryServiceBean
 */
@Stateless
public class InventoryServiceBean implements InventoryService {

	@PersistenceContext private EntityManager entitymanager;
	private static final String MY_QUERY = "Select i from Item i";
	
    public InventoryServiceBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public Inventory getAvailableInventory() {
		return new Inventory(entitymanager.createQuery(MY_QUERY, Item.class).getResultList());
	}

	@Override
	public boolean validateQuantity(Collection<LineItem> items) {
		List<Item> tmpList = this.getAvailableInventory().getInventory();
		int i = 0;
		for (LineItem item : items) {
			if (item.getQuantity() > tmpList.get(i).getQuantity()) 
				return false;
		}
		return true;
	}

	@Override
	public boolean updateInventory(Collection<Item> items) {
		// TODO Auto-generated method stub
		return true;
	}

}
